<%@page import="com.wide.ecommerce.domain.Product"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="Style.css"> 
<head>

<meta charset="ISO-8859-1">
<title> Product List </title>
</head>
<body>
<h1 class = "title">Product List </h1>
<%

%>
<a class = "link-product" href="product.do?action=view">New Product</a>

<table class="table-product" border="10" width="80%" > 
<thead>
<tr>
<th>No.</th>
<th>Code</th>
<th>Name</th>
<th>Type</th>
<th>Price</th>
<th>Action</th>
</tr>
</thead>
<c:forEach items="${product_data }" var="prod"> 
<tr>
		<td><c:out value="${prod.id}"/> </td>		
		<td><c:out value="${prod.code}"/> </td>
		<td><c:out value="${prod.name}"/> </td>
		<td><c:out value="${prod.type}"/> </td>
		<td><c:out value="${prod.price}"/> </td>
		<td><a href="/WideCommerce/product.do?action=edit&code=<c:out value="${prod.code}"/>">Update</a> &nbsp; 
		<a href="/WideCommerce/product.do?action=delete&code=<c:out value="${prod.code}"/>">Delete</a> </td>
</tr>		
</c:forEach>	

		
		

</table>



</body>
</html>