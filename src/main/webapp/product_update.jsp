<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="Style.css">
<head>
<meta charset="ISO-8859-1">
<title>Update Products Form</title>
</head>
<body>
<h1 class="title"> Update Product Form </h1>

<form action = "product.do?action=edit" method="post">
Code <br/>
<input type="text" name = "code" required value="${product.code}"/>
<br/>
Name <br/>
<input type="text" name = "name" required="required"/>
<br/>
Type <br/>
<input type="text" name = "type" required="required"/>
<br/>
Price <br/>
<input type="text" name = "price" required="required"/>
<br/>
<input class = "button" type="submit" name = "action" value = "update" >
<br/>
</form>
</body>
</html>