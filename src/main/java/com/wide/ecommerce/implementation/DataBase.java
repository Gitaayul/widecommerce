package com.wide.ecommerce.implementation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase {
	static String jdbcUrl = "jdbc:mysql://localhost:3306/training_db";
	static String username = "root";
	static String password = "";
	
	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//get connection 
			conn = DriverManager.getConnection(jdbcUrl, username, password);
			//System.out.println("Connection Sukses");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return conn;
		
	}
}
