package com.wide.ecommerce.implementation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.List;
import java.util.ArrayList;
import com.wide.ecommerce.domain.Product;
import com.wide.ecommerce.repositories.ProductRepository;

public class ProductRepositoryMySql implements ProductRepository{

	private Connection conn;



	public ProductRepositoryMySql() {
		super();
	}
	public ProductRepositoryMySql(Connection conn) {
		this.conn = conn;
	}
	@Override
	public List<Product> findAll() {
		List<Product> product = new ArrayList<Product>();

		//driver registration 
		try {

			Connection conn = DataBase.getConnection();

			String sqlQuery = "SELECT * FROM product_tbl";
			PreparedStatement stm = conn.prepareStatement(sqlQuery);
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String type = rs.getString("type");
				double price = rs.getDouble("price");
				String code = rs.getString("code");

				Product item = new Product(id, name, type, price, code);
				product.add(item);

			} 


		} catch (SQLException e) {
			System.out.println("Database error !");
			e.printStackTrace();
		}	

		return product;
	}

	@Override
	public Product findByCode(String code) {
		Product product = null;


		//driver registration 
		try {

			Connection conn = DataBase.getConnection();

			String sqlQuery = "SELECT * FROM product_tbl WHERE code = ? ";
			PreparedStatement stm = conn.prepareStatement(sqlQuery);
			stm.setString(1, code);
			ResultSet rs = stm.executeQuery();


			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String type = rs.getString("type");
				double price = rs.getDouble("price");
				String codepr = rs.getString("code");

				product = new Product(id, name, type, price, codepr);


			} 



		} catch (SQLException e) {
			System.out.println("Database error !");
			e.printStackTrace();
		}	
		return product;
	}

	@Override
	public int save(Product product) {

		int result = 0;
		//driver registration 
		try {

			Connection conn = DataBase.getConnection();


			String sql = "INSERT INTO  product_tbl (name, type, price, code) VALUES (?,?,?,?)";
			PreparedStatement prepStm = conn.prepareStatement(sql);
			prepStm.setString(1, product.getName());
			prepStm.setString(2, product.getType());
			prepStm.setDouble(3, product.getPrice());
			prepStm.setString(4, product.getCode());

			result = prepStm.executeUpdate();



		} catch (SQLException e) {
			System.out.println("Database error !");
			e.printStackTrace();
		}	
		return result;
	}
	@Override
	public int updateProduct(Product product) {
		int update = 0;
		try {
			Connection conn = DataBase.getConnection();
			String sql = "UPDATE product_tbl SET name = ?,type = ?,price=? WHERE code=? ";
			PreparedStatement prepared = conn.prepareStatement(sql);
			prepared.setString(1, product.getName());
			prepared.setString(2, product.getType());
			prepared.setDouble(3, product.getPrice());
			prepared.setString(4, product.getCode());


			update = prepared.executeUpdate();


		} catch (SQLException e) {
			System.out.println("Database error !");
			e.printStackTrace();
		}
		return update;
	}
	@Override
	public void deleteByCode(String code) {

		try {
			Connection conn = DataBase.getConnection();
			String sql = "DELETE FROM product_tbl WHERE code=?";
			PreparedStatement prepared = conn.prepareStatement(sql);
			prepared.setString(1, code);
			prepared.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		}

	}	
}