package com.wide.ecommerce.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wide.ecommerce.domain.Product;
import com.wide.ecommerce.implementation.DataBase;
import com.wide.ecommerce.implementation.ProductRepositoryMySql;
import com.wide.ecommerce.repositories.ProductRepository;

/**
 * Servlet implementation class ProductController
 */
@WebServlet("/product.do")
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProductRepository repo = new ProductRepositoryMySql(DataBase.getConnection());
		
		String action = request.getParameter("action");
		if ("view".equals(action)) {
			request.getRequestDispatcher("product_form.jsp").forward(request, response);
		}else if("edit".equals(action)) {
			String code =  request.getParameter("code");
			Product product = repo.findByCode(code);
			request.setAttribute("product", product);
			request.getRequestDispatcher("product_update.jsp").forward(request, response);
		
		}else if("delete".equals(action)){
			String code = request.getParameter("code");
			Product product = repo.findByCode(code);
			request.setAttribute("product", product);
			request.getRequestDispatcher("delete_product.jsp").forward(request, response);
			
		}
		else {
			
			List<Product> products = repo.findAll();
			request.setAttribute("product_data", products);
			request.getRequestDispatcher("product_list.jsp").forward(request, response);
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		
		if ("delete".equals(action)) {
		      ProductRepositoryMySql repos = new ProductRepositoryMySql();
		      repos.deleteByCode(request.getParameter("code"));
		      request.getRequestDispatcher("delete_product.jsp").include(request, response);
		      response.sendRedirect("product.do");
		}else if("edit".equals(action)){
			String code = request.getParameter("code");
			String name = request.getParameter("name");
			String type = request.getParameter("type");
			double price = Double.parseDouble(request.getParameter("price"));
			ProductRepository repository = new ProductRepositoryMySql(DataBase.getConnection());
			Product product = new Product(0,name,type,price,code);
			int update= repository.updateProduct(product);
			
			request.getRequestDispatcher("product_update.jsp").include(request, response);
			response.sendRedirect("product.do");
		}else {
			String code = request.getParameter("code");
			String name = request.getParameter("name");
			String type = request.getParameter("type");
			double price = Double.parseDouble(request.getParameter("price"));
			ProductRepository repo = new ProductRepositoryMySql(DataBase.getConnection());
			Product product = new Product(0, name, type, price, code);
			
			int result = repo.save(product);
			
			response.sendRedirect("product.do");
			
		}
		
		//doGet(request, response);
	}}


