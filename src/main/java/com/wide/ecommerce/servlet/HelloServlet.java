package com.wide.ecommerce.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wide.ecommerce.domain.Product;
import com.wide.ecommerce.implementation.DataBase;
import com.wide.ecommerce.implementation.ProductRepositoryMySql;
import com.wide.ecommerce.repositories.ProductRepository;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/HelloServlet")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		ProductRepository repo = new ProductRepositoryMySql(DataBase.getConnection());
		List<Product> products = repo.findAll();
		
		out.println("<table border=10 width=70%>");
		for (Product p : products) {
			out.println("<tr>");
			//out.println("<td> Id : " +p.getId()+ "</td>");
			out.println("<td> Code : " +p.getCode()+ "</td>");
			out.println("<td> Name : " +p.getName() + "</td>"); 
			out.println("<td> Type : " + p.getType() + "</td>" );
			out.println("<td> Price : " + p.getPrice() + "</td>");
			
			
			out.println("</tr>");
		
		}
		out.println("</table>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		PrintWriter out = response.getWriter();
		
//		String id = request.getParameter("id");
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String type = request.getParameter("type");
		Double price =Double.parseDouble(request.getParameter("price")) ;
		
		Product product = new Product(1, name, type,price,code );
		ProductRepository repo = new ProductRepositoryMySql(DataBase.getConnection());
		
		int result = repo.save(product);
		
		
		
		if (result >0) {
			
			out.println("<h1> Sukses Insert Data </h1>");
		} 
		else {
			out.println("<h1> Gagal Insert Data </h1>");
		}
//		out.println("<h1> Hello From Servlet, Welcome" +name +"</h1>");
//		out.println("<h1> Your Adress is " + address +"</h1>");
//		out.println("<h1> Your Age is " + age +"</h1>");
	}

}